# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tdemay <tdemay@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/05 15:37:22 by tdemay            #+#    #+#              #
#    Updated: 2014/02/05 15:37:22 by tdemay           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: clean fclean re

NAME = lemin

FLAGS = -Wall -Wextra -Werror

LIB = -Llibft/ -lft

SRC =

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): libft/libft.a $(NAME).c  $(NAME).h $(OBJ)
	@printf "\r\033[38;5;11m⌛  MAKE   $(NAME) plz wait ...\033[K\033[0m"
	@gcc $(FLAGS) $(LIB) $(OBJ) $(NAME).c -o $(NAME)
	@echo -en "\r\033[38;5;22m✅  MAKE   $(NAME)\033[K\033[0m"

./libft/libft.a:
	@make -C libft/ fclean
	@make -C libft/

%.o: %.c %.h
	@gcc $(FLAGS) -c $< -o $@

clean:
	@printf "\r\033[38;5;25m⌛  CLEAN  $(NAME) plz wait ...\033[K\033[0m"
	@rm -f $(OBJ)
	@echo -en "\r\033[38;5;124m❌  CLEAN  $(NAME)\033[K\033[0m"

fclean: clean
	@printf "\r\033[38;5;25m⌛  FCLEAN $(NAME) plz wait ...\033[K\033[0m"
	@rm -f $(NAME)
	@make -C ./libft/ fclean
	@echo -en "\r\033[38;5;124m❌  FCLEAN $(NAME)\033[K\033[0m"

re: fclean all
