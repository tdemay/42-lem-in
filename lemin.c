/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdemay <tdemay@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/06 19:17:54 by tdemay            #+#    #+#             */
/*   Updated: 2014/02/06 19:17:54 by tdemay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	get_info(t_env *e)
{
	int		finish;
	char	*ligne;
	char	**get;

	get_next_line(0, &ligne);
	e->fourmi = ft_atoi(ft_strsplit(ligne, ' ')[0]);
	finish = 1;
	while (finish && get_next_line(0, &ligne) > 0)
	{
		if (ft_strstr(ligne, "##start") && get_next_line(0, &ligne) > 0)
		{
			get = ft_strsplit(ligne, ' ');
			add_lst(e->root, get[0], get[1], get[2], START);
		}
		else if (ft_strstr(ligne, "##end") && get_next_line(0, &ligne) > 0)
		{
			get = ft_strsplit(ligne, ' ');
			add_lst(e->root, get[0], get[1], get[2], START);
		}
		else
		{
			get = ft_strsplit(ligne, ' ');
			add_lst(e->root, get[0], get[1], get[2], NODE);
		}
	}
}

int		main(void)
{
	t_env	e;

	get_info(&e);
	return (0);
}
